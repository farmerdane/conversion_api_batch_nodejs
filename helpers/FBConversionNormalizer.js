const rfr = require('rfr');
const adsSdk = require('facebook-nodejs-business-sdk');
const strtotime = require('locutus/php/datetime/strtotime');

class FBConversionNormalizer {

    constructor(data, getters) {
        this.data = data;
        this.getters = getters;
    }

    getCustomProperties(data, props) {
        let finaldata = {};
        for (let i in props) {
            if (data[props[i]] !== undefined) {
                finaldata[props[i]] = data[props[i]];
            }
        }

        return finaldata;
    }

    async translateNormalizeData(event_name, data, actionsource) {
        return await this.normalize(event_name, data.eventId, data.timestamp, data.pageUrl, this.getCustomProperties(data, ["value","currency"]), actionsource);
    }

    async normalize(eventname, eventid, eventdate, eventurl, customProperties, actionsource) {
        const seven_days_ago = strtotime("-7 days");

        let final_data = {};

        for (let k in this.getters) {
            let v = this.getters[k];
            if (this.data[v]) {
                if (
                    (v === 'fbc' || v === 'fbp') && this.data[v].indexOf('fb.') < 0 ||
                    (v === 'fbc' || v === 'fbp') && this.data[v].length < 20
                ) {
                    //scrap non-valid fbc-fbp
                } else {
                    final_data[k] = this.data[v];
                }
            }
        }

        // there are individul "settter" functions... Just using the constructor for speed...
        //UserData(email, phone, gender, first_name, last_name, date_of_birth, city, state, zip, country, external_id, client_ip_address, client_user_agent, fbp, fbc, subscription_id, fb_login_id, lead_id, dobd, dobm, doby)
        let user_data = new adsSdk.UserData(
            final_data['email'], //email,
            final_data['phone'], // phone, 
            null, // gender, 
            final_data['first_name'], // first_name, 
            final_data['last_name'], // last_name, 
            null, // date_of_birth, 
            null, // city, 
            null, // state, 
            null, // zip, 
            final_data['country_code'], // country, 
            final_data['external_id'], // external_id, 
            final_data['client_ip_address'], // client_ip_address, 
            final_data['client_user_agent'], // client_user_agent
            final_data['fbp'], // fbp, 
            final_data['fbc'], //fbc, 
            null, // subscription_id, 
            null, // fb_login_id, 
            null, // lead_id, 
            null, // dobd, 
            null, // dobm, 
            null, // doby
        );
        //console.log(user_data);

        let custom_data = (new adsSdk.CustomData())
            .setCustomProperties(customProperties);



        let datadate = strtotime(eventdate);
        if (datadate < seven_days_ago) {
            throw new Error("Event Exceeds 7 days in the past");
        }

        let parsedactionsource = false;
        if (actionsource) {
            parsedactionsource = actionsource;
        } else {
            if (final_data['client_ip_address'] && final_data['client_user_agent']) {
                parsedactionsource = 'website';
            } else {
                parsedactionsource = 'other';
            }
        }

        let event = (new adsSdk.ServerEvent())
            .setEventName(eventname)
            .setEventTime(datadate)
            .setEventId(eventid)
            .setEventSourceUrl(eventurl)
            .setActionSource(parsedactionsource)
            .setUserData(user_data)
            .setCustomData(custom_data);

        return event.normalize();

    }
}

module.exports = FBConversionNormalizer;