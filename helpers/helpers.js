
const axios = require('axios');
const axiosRetry = require('axios-retry');
const http = require('http');
const https = require('https');

class helpers {

    static createAxios(useragent) {
        let axconn = axios.create({
            httpAgent: new http.Agent({ keepAlive: true }),
            httpsAgent: new https.Agent({ keepAlive: true }),
            headers: {
                'User-Agent': useragent
            }
        });
        axiosRetry(axconn, { retryDelay: axiosRetry.exponentialDelay });
        return axconn;
    }

    static getArrayChunks(array, chunk) {
        let i, j, temparray;
        let chunks = [];
        for (i = 0, j = array.length; i < j; i += chunk) {
            chunks.push(array.slice(i, i + chunk));
        }

        return chunks;
    }

    static async sleep(millis) {
        return new Promise(resolve => setTimeout(resolve, millis));
    }
}

module.exports = helpers;