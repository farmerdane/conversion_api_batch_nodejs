const rfr = require('rfr');
const helpers = rfr('helpers/helpers');

class FBConversionApiConnection {
  constructor(url, method) {
    this.url = url;
    this.method = method;
    this.axios = helpers.createAxios();
    this.testid = null;
  }

  setTestId(testid) {
    this.testid = testid;
  }

  async send(conv_events, access_token, delay) {

    const json = JSON.stringify(conv_events);

    let options = {
      method: this.method,
      url: this.url,
      data: {
        access_token: access_token,
        data: json,
        test_event_code: this.testid
      },
      headers: { "Content-Type": "application/json" }
    };

    try {
      const res = await this.axios(options);
      let topts = { test_event_code: this.testid, url: this.url };
      return {
        ...topts,
        ...res.data
      };
    } catch (error) {
      let errorResponse;
      //console.log(error);
      if (error.response && error.response.data) {
        // I expect the API to handle error responses in valid format
        errorResponse = error.response.data;
        // JSON stringify if you need the json and use it later
      } else if (error.request) {
        // TO Handle the default error response for Network failure or 404 etc.,
        errorResponse = error.request.message || error.request.statusText;
      } else {
        errorResponse = error.message;
      }
      console.log(errorResponse);
    }


    await helpers.sleep(delay);
    return null;
  }
}

module.exports = FBConversionApiConnection;