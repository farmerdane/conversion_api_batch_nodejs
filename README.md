
# Sample Node.JS Conversion API Integration

## Requirements
- Node 13+
- Facebook Business SDK for NodeJS v12+

## Facebook Business SDK for Node.JS - Library Dependencies

```
npm install --save facebook-nodejs-business-sdk
```

https://github.com/facebook/facebook-nodejs-business-sdk

## Config File
A `config.json` file needs to be created as it contains the settings for the access tokens and pixel IDs.

```
{
  "graph_url": "https://graph.facebook.com/{{version}}/{{pixel_id}}/events",
  "version": "v12.0",
  "access_token": "<ACCESS_TOKEN>",
  "pixel_id": "<PIXEL_ID>",
  "test_id": "<TEST_ID>"
}
```

## To Run

Add libraries:
```
npm update
```

Run application:
```
node multi_conversion.js
```