const fs = require('fs');
const rfr = require('rfr');
const helpers = rfr('helpers/helpers');
const adsSdk = require('facebook-nodejs-business-sdk');

const FBConversionApiConnection = rfr('helpers/FBConversionApiConnection');
const FBConversionNormalizer = rfr('helpers/FBConversionNormalizer');
const config = rfr("./config.json");

const main = async () => {
    const ts = Math.round(new Date() / 1000);

    let data = [{
        "country_code": "GB",
        "forename": "ALastName",
        "surname": "AFirstName",
        "fbp": "fb.1.1606131921797.1732594006",
        "fbc": "fb.1.1610489108757.IwAR272H8OhxvlfPbJK3-Iyj2GCC7zW_eJc7pgyXZwoBO19fEF8yB8NoiBaxI",
        "eventId": "d9d01dc0-36e5-430b-b731-952e7b4b8fcc",
        "email": "test@example3.com",
        "phone": "07531111111",
        "client_user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36 Edg/88.0.705.63",
        "client_ip_address": "2.28.48.145",
        "value": 123.00,
        "currency": "EUR",
        "timestamp": ts,
        "pageUrl": "https://someurl.com/go/",
    },
    {
        "country_code": "NO",
        "forename": "Afn",
        "surname": "Aln",
        "fbp": "fb.1.1606131921797.1732594006",
        "fbc": "fb.1.1610489108757.IwAR272H8OhxvlfPbJK3-Iyj2GCC7zW_eJc7pgyXZwoBO19fEF8yB8NoiBaxI",
        "eventId": "b43gesd-42e5-430b-b731-165e3nd94cdy",
        "email": "test0@ex2223.com",
        "phone": "07531111122",
        "client_user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36 Edg/88.0.705.63",
        "client_ip_address": "2.28.48.145",
        "value": 23.00,
        "currency": "NOK",
        "timestamp": ts,
        "pageUrl": "https://someurl.com/go/",
    }];

    // Conversion API can take a max of 1000 events in batch at once
    const chunks = helpers.getArrayChunks(data, 1000);

    // mapping just exists to extract values from a custom JSON object to the required FB event keys
    // left side is FB's property right side is the key within the data payload
    const getters = {
        'email': 'email',
        'phone': 'phone',
        'gender': false,
        'date_of_birth': false,
        'last_name': 'surname',
        'first_name': 'forename',
        'city': false,
        'state': false,
        'country_code': false,
        'zip_code': 'postcode',
        'external_id': 'eventId',
        'client_ip_address': 'client_ip_address',
        'client_user_agent': 'client_user_agent',
        'fbc': "fbc",
        'fbp': "fbp",
        'subscription_id': false,
        'fb_login_id': false,
        'lead_id': false,
        'f5first': false,
        'f5last': false,
        'fi': false,
        'dobd': false,
        'dobm': false,
        'doby': false,
    };

    // loop through the batches of events
    for (let i in chunks) {
        try {
            let conv_events = [];

            // for each element in the batch normalize the values and prepare to send
            for (j in chunks[i]) {
                let anevent = chunks[i][j];
                let fcn = new FBConversionNormalizer(anevent, getters);
                let event = await fcn.translateNormalizeData("Purchase", anevent);
                conv_events.push(event);
            }

            const fb_url = config.graph_url.replace("{{version}}", config.version).replace("{{pixel_id}}", config.pixel_id);
            let fac = new FBConversionApiConnection(
                fb_url,
                "POST"
            );
            fac.setTestId(config.test_id);
            //send the batch of events
            const ret = await fac.send(conv_events, config.access_token, 0);
            console.log(JSON.stringify(conv_events), ret);
        } catch (e) {
            console.log(e);
        }
    }




};

main();